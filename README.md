# convertables

Convertable values to store such things as Angles, Distances, Speeds, etc. 

The main goal is to have a useful base class to have such a convertable values:

TAngle a<deg>(90);
a.toRad() == M_PI / 2;